package ru.app.jhapptest.web.rest;

import ru.app.jhapptest.JhapptestApp;
import ru.app.jhapptest.domain.Emploee;
import ru.app.jhapptest.repository.EmploeeRepository;
import ru.app.jhapptest.service.EmploeeService;
import ru.app.jhapptest.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.app.jhapptest.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link EmploeeResource} REST controller.
 */
@SpringBootTest(classes = JhapptestApp.class)
public class EmploeeResourceIT {

    private static final String DEFAULT_FIO = "AAAAAAAAAA";
    private static final String UPDATED_FIO = "BBBBBBBBBB";

    @Autowired
    private EmploeeRepository emploeeRepository;

    @Autowired
    private EmploeeService emploeeService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restEmploeeMockMvc;

    private Emploee emploee;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EmploeeResource emploeeResource = new EmploeeResource(emploeeService);
        this.restEmploeeMockMvc = MockMvcBuilders.standaloneSetup(emploeeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Emploee createEntity(EntityManager em) {
        Emploee emploee = new Emploee()
            .fio(DEFAULT_FIO);
        return emploee;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Emploee createUpdatedEntity(EntityManager em) {
        Emploee emploee = new Emploee()
            .fio(UPDATED_FIO);
        return emploee;
    }

    @BeforeEach
    public void initTest() {
        emploee = createEntity(em);
    }

    @Test
    @Transactional
    public void createEmploee() throws Exception {
        int databaseSizeBeforeCreate = emploeeRepository.findAll().size();

        // Create the Emploee
        restEmploeeMockMvc.perform(post("/api/emploees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(emploee)))
            .andExpect(status().isCreated());

        // Validate the Emploee in the database
        List<Emploee> emploeeList = emploeeRepository.findAll();
        assertThat(emploeeList).hasSize(databaseSizeBeforeCreate + 1);
        Emploee testEmploee = emploeeList.get(emploeeList.size() - 1);
        assertThat(testEmploee.getFio()).isEqualTo(DEFAULT_FIO);
    }

    @Test
    @Transactional
    public void createEmploeeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = emploeeRepository.findAll().size();

        // Create the Emploee with an existing ID
        emploee.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEmploeeMockMvc.perform(post("/api/emploees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(emploee)))
            .andExpect(status().isBadRequest());

        // Validate the Emploee in the database
        List<Emploee> emploeeList = emploeeRepository.findAll();
        assertThat(emploeeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllEmploees() throws Exception {
        // Initialize the database
        emploeeRepository.saveAndFlush(emploee);

        // Get all the emploeeList
        restEmploeeMockMvc.perform(get("/api/emploees?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(emploee.getId().intValue())))
            .andExpect(jsonPath("$.[*].fio").value(hasItem(DEFAULT_FIO.toString())));
    }
    
    @Test
    @Transactional
    public void getEmploee() throws Exception {
        // Initialize the database
        emploeeRepository.saveAndFlush(emploee);

        // Get the emploee
        restEmploeeMockMvc.perform(get("/api/emploees/{id}", emploee.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(emploee.getId().intValue()))
            .andExpect(jsonPath("$.fio").value(DEFAULT_FIO.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingEmploee() throws Exception {
        // Get the emploee
        restEmploeeMockMvc.perform(get("/api/emploees/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEmploee() throws Exception {
        // Initialize the database
        emploeeService.save(emploee);

        int databaseSizeBeforeUpdate = emploeeRepository.findAll().size();

        // Update the emploee
        Emploee updatedEmploee = emploeeRepository.findById(emploee.getId()).get();
        // Disconnect from session so that the updates on updatedEmploee are not directly saved in db
        em.detach(updatedEmploee);
        updatedEmploee
            .fio(UPDATED_FIO);

        restEmploeeMockMvc.perform(put("/api/emploees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedEmploee)))
            .andExpect(status().isOk());

        // Validate the Emploee in the database
        List<Emploee> emploeeList = emploeeRepository.findAll();
        assertThat(emploeeList).hasSize(databaseSizeBeforeUpdate);
        Emploee testEmploee = emploeeList.get(emploeeList.size() - 1);
        assertThat(testEmploee.getFio()).isEqualTo(UPDATED_FIO);
    }

    @Test
    @Transactional
    public void updateNonExistingEmploee() throws Exception {
        int databaseSizeBeforeUpdate = emploeeRepository.findAll().size();

        // Create the Emploee

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEmploeeMockMvc.perform(put("/api/emploees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(emploee)))
            .andExpect(status().isBadRequest());

        // Validate the Emploee in the database
        List<Emploee> emploeeList = emploeeRepository.findAll();
        assertThat(emploeeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEmploee() throws Exception {
        // Initialize the database
        emploeeService.save(emploee);

        int databaseSizeBeforeDelete = emploeeRepository.findAll().size();

        // Delete the emploee
        restEmploeeMockMvc.perform(delete("/api/emploees/{id}", emploee.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Emploee> emploeeList = emploeeRepository.findAll();
        assertThat(emploeeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Emploee.class);
        Emploee emploee1 = new Emploee();
        emploee1.setId(1L);
        Emploee emploee2 = new Emploee();
        emploee2.setId(emploee1.getId());
        assertThat(emploee1).isEqualTo(emploee2);
        emploee2.setId(2L);
        assertThat(emploee1).isNotEqualTo(emploee2);
        emploee1.setId(null);
        assertThat(emploee1).isNotEqualTo(emploee2);
    }
}
