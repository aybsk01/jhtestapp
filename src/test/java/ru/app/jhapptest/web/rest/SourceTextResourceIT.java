package ru.app.jhapptest.web.rest;

import ru.app.jhapptest.JhapptestApp;
import ru.app.jhapptest.domain.SourceText;
import ru.app.jhapptest.repository.SourceTextRepository;
import ru.app.jhapptest.service.SourceTextService;
import ru.app.jhapptest.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.app.jhapptest.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SourceTextResource} REST controller.
 */
@SpringBootTest(classes = JhapptestApp.class)
public class SourceTextResourceIT {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_CONTENT = "AAAAAAAAAA";
    private static final String UPDATED_CONTENT = "BBBBBBBBBB";

    @Autowired
    private SourceTextRepository sourceTextRepository;

    @Autowired
    private SourceTextService sourceTextService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSourceTextMockMvc;

    private SourceText sourceText;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SourceTextResource sourceTextResource = new SourceTextResource(sourceTextService);
        this.restSourceTextMockMvc = MockMvcBuilders.standaloneSetup(sourceTextResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SourceText createEntity(EntityManager em) {
        SourceText sourceText = new SourceText()
            .title(DEFAULT_TITLE)
            .content(DEFAULT_CONTENT);
        return sourceText;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SourceText createUpdatedEntity(EntityManager em) {
        SourceText sourceText = new SourceText()
            .title(UPDATED_TITLE)
            .content(UPDATED_CONTENT);
        return sourceText;
    }

    @BeforeEach
    public void initTest() {
        sourceText = createEntity(em);
    }

    @Test
    @Transactional
    public void createSourceText() throws Exception {
        int databaseSizeBeforeCreate = sourceTextRepository.findAll().size();

        // Create the SourceText
        restSourceTextMockMvc.perform(post("/api/source-texts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sourceText)))
            .andExpect(status().isCreated());

        // Validate the SourceText in the database
        List<SourceText> sourceTextList = sourceTextRepository.findAll();
        assertThat(sourceTextList).hasSize(databaseSizeBeforeCreate + 1);
        SourceText testSourceText = sourceTextList.get(sourceTextList.size() - 1);
        assertThat(testSourceText.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testSourceText.getContent()).isEqualTo(DEFAULT_CONTENT);
    }

    @Test
    @Transactional
    public void createSourceTextWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = sourceTextRepository.findAll().size();

        // Create the SourceText with an existing ID
        sourceText.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSourceTextMockMvc.perform(post("/api/source-texts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sourceText)))
            .andExpect(status().isBadRequest());

        // Validate the SourceText in the database
        List<SourceText> sourceTextList = sourceTextRepository.findAll();
        assertThat(sourceTextList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllSourceTexts() throws Exception {
        // Initialize the database
        sourceTextRepository.saveAndFlush(sourceText);

        // Get all the sourceTextList
        restSourceTextMockMvc.perform(get("/api/source-texts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sourceText.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].content").value(hasItem(DEFAULT_CONTENT.toString())));
    }
    
    @Test
    @Transactional
    public void getSourceText() throws Exception {
        // Initialize the database
        sourceTextRepository.saveAndFlush(sourceText);

        // Get the sourceText
        restSourceTextMockMvc.perform(get("/api/source-texts/{id}", sourceText.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(sourceText.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.content").value(DEFAULT_CONTENT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSourceText() throws Exception {
        // Get the sourceText
        restSourceTextMockMvc.perform(get("/api/source-texts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSourceText() throws Exception {
        // Initialize the database
        sourceTextService.save(sourceText);

        int databaseSizeBeforeUpdate = sourceTextRepository.findAll().size();

        // Update the sourceText
        SourceText updatedSourceText = sourceTextRepository.findById(sourceText.getId()).get();
        // Disconnect from session so that the updates on updatedSourceText are not directly saved in db
        em.detach(updatedSourceText);
        updatedSourceText
            .title(UPDATED_TITLE)
            .content(UPDATED_CONTENT);

        restSourceTextMockMvc.perform(put("/api/source-texts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSourceText)))
            .andExpect(status().isOk());

        // Validate the SourceText in the database
        List<SourceText> sourceTextList = sourceTextRepository.findAll();
        assertThat(sourceTextList).hasSize(databaseSizeBeforeUpdate);
        SourceText testSourceText = sourceTextList.get(sourceTextList.size() - 1);
        assertThat(testSourceText.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testSourceText.getContent()).isEqualTo(UPDATED_CONTENT);
    }

    @Test
    @Transactional
    public void updateNonExistingSourceText() throws Exception {
        int databaseSizeBeforeUpdate = sourceTextRepository.findAll().size();

        // Create the SourceText

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSourceTextMockMvc.perform(put("/api/source-texts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sourceText)))
            .andExpect(status().isBadRequest());

        // Validate the SourceText in the database
        List<SourceText> sourceTextList = sourceTextRepository.findAll();
        assertThat(sourceTextList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSourceText() throws Exception {
        // Initialize the database
        sourceTextService.save(sourceText);

        int databaseSizeBeforeDelete = sourceTextRepository.findAll().size();

        // Delete the sourceText
        restSourceTextMockMvc.perform(delete("/api/source-texts/{id}", sourceText.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SourceText> sourceTextList = sourceTextRepository.findAll();
        assertThat(sourceTextList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SourceText.class);
        SourceText sourceText1 = new SourceText();
        sourceText1.setId(1L);
        SourceText sourceText2 = new SourceText();
        sourceText2.setId(sourceText1.getId());
        assertThat(sourceText1).isEqualTo(sourceText2);
        sourceText2.setId(2L);
        assertThat(sourceText1).isNotEqualTo(sourceText2);
        sourceText1.setId(null);
        assertThat(sourceText1).isNotEqualTo(sourceText2);
    }
}
