package ru.app.jhapptest.web.rest;

import ru.app.jhapptest.JhapptestApp;
import ru.app.jhapptest.domain.TranslatedText;
import ru.app.jhapptest.repository.TranslatedTextRepository;
import ru.app.jhapptest.service.TranslatedTextService;
import ru.app.jhapptest.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.app.jhapptest.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import ru.app.jhapptest.domain.enumeration.Language;
/**
 * Integration tests for the {@link TranslatedTextResource} REST controller.
 */
@SpringBootTest(classes = JhapptestApp.class)
public class TranslatedTextResourceIT {

    private static final String DEFAULT_CONTENT = "AAAAAAAAAA";
    private static final String UPDATED_CONTENT = "BBBBBBBBBB";

    private static final Language DEFAULT_LANGUAGE = Language.ENGLISH;
    private static final Language UPDATED_LANGUAGE = Language.RUSSIAN;

    @Autowired
    private TranslatedTextRepository translatedTextRepository;

    @Autowired
    private TranslatedTextService translatedTextService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTranslatedTextMockMvc;

    private TranslatedText translatedText;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TranslatedTextResource translatedTextResource = new TranslatedTextResource(translatedTextService);
        this.restTranslatedTextMockMvc = MockMvcBuilders.standaloneSetup(translatedTextResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TranslatedText createEntity(EntityManager em) {
        TranslatedText translatedText = new TranslatedText()
            .content(DEFAULT_CONTENT)
            .language(DEFAULT_LANGUAGE);
        return translatedText;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TranslatedText createUpdatedEntity(EntityManager em) {
        TranslatedText translatedText = new TranslatedText()
            .content(UPDATED_CONTENT)
            .language(UPDATED_LANGUAGE);
        return translatedText;
    }

    @BeforeEach
    public void initTest() {
        translatedText = createEntity(em);
    }

    @Test
    @Transactional
    public void createTranslatedText() throws Exception {
        int databaseSizeBeforeCreate = translatedTextRepository.findAll().size();

        // Create the TranslatedText
        restTranslatedTextMockMvc.perform(post("/api/translated-texts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(translatedText)))
            .andExpect(status().isCreated());

        // Validate the TranslatedText in the database
        List<TranslatedText> translatedTextList = translatedTextRepository.findAll();
        assertThat(translatedTextList).hasSize(databaseSizeBeforeCreate + 1);
        TranslatedText testTranslatedText = translatedTextList.get(translatedTextList.size() - 1);
        assertThat(testTranslatedText.getContent()).isEqualTo(DEFAULT_CONTENT);
        assertThat(testTranslatedText.getLanguage()).isEqualTo(DEFAULT_LANGUAGE);
    }

    @Test
    @Transactional
    public void createTranslatedTextWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = translatedTextRepository.findAll().size();

        // Create the TranslatedText with an existing ID
        translatedText.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTranslatedTextMockMvc.perform(post("/api/translated-texts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(translatedText)))
            .andExpect(status().isBadRequest());

        // Validate the TranslatedText in the database
        List<TranslatedText> translatedTextList = translatedTextRepository.findAll();
        assertThat(translatedTextList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTranslatedTexts() throws Exception {
        // Initialize the database
        translatedTextRepository.saveAndFlush(translatedText);

        // Get all the translatedTextList
        restTranslatedTextMockMvc.perform(get("/api/translated-texts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(translatedText.getId().intValue())))
            .andExpect(jsonPath("$.[*].content").value(hasItem(DEFAULT_CONTENT.toString())))
            .andExpect(jsonPath("$.[*].language").value(hasItem(DEFAULT_LANGUAGE.toString())));
    }
    
    @Test
    @Transactional
    public void getTranslatedText() throws Exception {
        // Initialize the database
        translatedTextRepository.saveAndFlush(translatedText);

        // Get the translatedText
        restTranslatedTextMockMvc.perform(get("/api/translated-texts/{id}", translatedText.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(translatedText.getId().intValue()))
            .andExpect(jsonPath("$.content").value(DEFAULT_CONTENT.toString()))
            .andExpect(jsonPath("$.language").value(DEFAULT_LANGUAGE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTranslatedText() throws Exception {
        // Get the translatedText
        restTranslatedTextMockMvc.perform(get("/api/translated-texts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTranslatedText() throws Exception {
        // Initialize the database
        translatedTextService.save(translatedText);

        int databaseSizeBeforeUpdate = translatedTextRepository.findAll().size();

        // Update the translatedText
        TranslatedText updatedTranslatedText = translatedTextRepository.findById(translatedText.getId()).get();
        // Disconnect from session so that the updates on updatedTranslatedText are not directly saved in db
        em.detach(updatedTranslatedText);
        updatedTranslatedText
            .content(UPDATED_CONTENT)
            .language(UPDATED_LANGUAGE);

        restTranslatedTextMockMvc.perform(put("/api/translated-texts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTranslatedText)))
            .andExpect(status().isOk());

        // Validate the TranslatedText in the database
        List<TranslatedText> translatedTextList = translatedTextRepository.findAll();
        assertThat(translatedTextList).hasSize(databaseSizeBeforeUpdate);
        TranslatedText testTranslatedText = translatedTextList.get(translatedTextList.size() - 1);
        assertThat(testTranslatedText.getContent()).isEqualTo(UPDATED_CONTENT);
        assertThat(testTranslatedText.getLanguage()).isEqualTo(UPDATED_LANGUAGE);
    }

    @Test
    @Transactional
    public void updateNonExistingTranslatedText() throws Exception {
        int databaseSizeBeforeUpdate = translatedTextRepository.findAll().size();

        // Create the TranslatedText

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTranslatedTextMockMvc.perform(put("/api/translated-texts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(translatedText)))
            .andExpect(status().isBadRequest());

        // Validate the TranslatedText in the database
        List<TranslatedText> translatedTextList = translatedTextRepository.findAll();
        assertThat(translatedTextList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTranslatedText() throws Exception {
        // Initialize the database
        translatedTextService.save(translatedText);

        int databaseSizeBeforeDelete = translatedTextRepository.findAll().size();

        // Delete the translatedText
        restTranslatedTextMockMvc.perform(delete("/api/translated-texts/{id}", translatedText.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TranslatedText> translatedTextList = translatedTextRepository.findAll();
        assertThat(translatedTextList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TranslatedText.class);
        TranslatedText translatedText1 = new TranslatedText();
        translatedText1.setId(1L);
        TranslatedText translatedText2 = new TranslatedText();
        translatedText2.setId(translatedText1.getId());
        assertThat(translatedText1).isEqualTo(translatedText2);
        translatedText2.setId(2L);
        assertThat(translatedText1).isNotEqualTo(translatedText2);
        translatedText1.setId(null);
        assertThat(translatedText1).isNotEqualTo(translatedText2);
    }
}
