import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { JhapptestTestModule } from '../../../test.module';
import { SourceTextDeleteDialogComponent } from 'app/entities/source-text/source-text-delete-dialog.component';
import { SourceTextService } from 'app/entities/source-text/source-text.service';

describe('Component Tests', () => {
  describe('SourceText Management Delete Component', () => {
    let comp: SourceTextDeleteDialogComponent;
    let fixture: ComponentFixture<SourceTextDeleteDialogComponent>;
    let service: SourceTextService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhapptestTestModule],
        declarations: [SourceTextDeleteDialogComponent]
      })
        .overrideTemplate(SourceTextDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SourceTextDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SourceTextService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
