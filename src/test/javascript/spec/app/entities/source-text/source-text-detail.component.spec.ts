import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { JhapptestTestModule } from '../../../test.module';
import { SourceTextDetailComponent } from 'app/entities/source-text/source-text-detail.component';
import { SourceText } from 'app/shared/model/source-text.model';

describe('Component Tests', () => {
  describe('SourceText Management Detail Component', () => {
    let comp: SourceTextDetailComponent;
    let fixture: ComponentFixture<SourceTextDetailComponent>;
    const route = ({ data: of({ sourceText: new SourceText(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhapptestTestModule],
        declarations: [SourceTextDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(SourceTextDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SourceTextDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.sourceText).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
