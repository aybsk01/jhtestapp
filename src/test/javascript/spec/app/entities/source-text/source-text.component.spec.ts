import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { JhapptestTestModule } from '../../../test.module';
import { SourceTextComponent } from 'app/entities/source-text/source-text.component';
import { SourceTextService } from 'app/entities/source-text/source-text.service';
import { SourceText } from 'app/shared/model/source-text.model';

describe('Component Tests', () => {
  describe('SourceText Management Component', () => {
    let comp: SourceTextComponent;
    let fixture: ComponentFixture<SourceTextComponent>;
    let service: SourceTextService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhapptestTestModule],
        declarations: [SourceTextComponent],
        providers: []
      })
        .overrideTemplate(SourceTextComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SourceTextComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SourceTextService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new SourceText(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.sourceTexts[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
