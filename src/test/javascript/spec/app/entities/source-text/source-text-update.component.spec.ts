import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { JhapptestTestModule } from '../../../test.module';
import { SourceTextUpdateComponent } from 'app/entities/source-text/source-text-update.component';
import { SourceTextService } from 'app/entities/source-text/source-text.service';
import { SourceText } from 'app/shared/model/source-text.model';

describe('Component Tests', () => {
  describe('SourceText Management Update Component', () => {
    let comp: SourceTextUpdateComponent;
    let fixture: ComponentFixture<SourceTextUpdateComponent>;
    let service: SourceTextService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhapptestTestModule],
        declarations: [SourceTextUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(SourceTextUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SourceTextUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SourceTextService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new SourceText(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new SourceText();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
