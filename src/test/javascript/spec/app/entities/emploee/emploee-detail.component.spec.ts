import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { JhapptestTestModule } from '../../../test.module';
import { EmploeeDetailComponent } from 'app/entities/emploee/emploee-detail.component';
import { Emploee } from 'app/shared/model/emploee.model';

describe('Component Tests', () => {
  describe('Emploee Management Detail Component', () => {
    let comp: EmploeeDetailComponent;
    let fixture: ComponentFixture<EmploeeDetailComponent>;
    const route = ({ data: of({ emploee: new Emploee(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhapptestTestModule],
        declarations: [EmploeeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(EmploeeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(EmploeeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.emploee).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
