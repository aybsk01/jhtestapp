import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { JhapptestTestModule } from '../../../test.module';
import { EmploeeUpdateComponent } from 'app/entities/emploee/emploee-update.component';
import { EmploeeService } from 'app/entities/emploee/emploee.service';
import { Emploee } from 'app/shared/model/emploee.model';

describe('Component Tests', () => {
  describe('Emploee Management Update Component', () => {
    let comp: EmploeeUpdateComponent;
    let fixture: ComponentFixture<EmploeeUpdateComponent>;
    let service: EmploeeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhapptestTestModule],
        declarations: [EmploeeUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(EmploeeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(EmploeeUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(EmploeeService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Emploee(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Emploee();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
