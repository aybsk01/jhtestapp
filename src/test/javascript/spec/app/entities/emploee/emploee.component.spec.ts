import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { JhapptestTestModule } from '../../../test.module';
import { EmploeeComponent } from 'app/entities/emploee/emploee.component';
import { EmploeeService } from 'app/entities/emploee/emploee.service';
import { Emploee } from 'app/shared/model/emploee.model';

describe('Component Tests', () => {
  describe('Emploee Management Component', () => {
    let comp: EmploeeComponent;
    let fixture: ComponentFixture<EmploeeComponent>;
    let service: EmploeeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhapptestTestModule],
        declarations: [EmploeeComponent],
        providers: []
      })
        .overrideTemplate(EmploeeComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(EmploeeComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(EmploeeService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Emploee(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.emploees[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
