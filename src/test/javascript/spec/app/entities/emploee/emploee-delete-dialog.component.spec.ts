import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { JhapptestTestModule } from '../../../test.module';
import { EmploeeDeleteDialogComponent } from 'app/entities/emploee/emploee-delete-dialog.component';
import { EmploeeService } from 'app/entities/emploee/emploee.service';

describe('Component Tests', () => {
  describe('Emploee Management Delete Component', () => {
    let comp: EmploeeDeleteDialogComponent;
    let fixture: ComponentFixture<EmploeeDeleteDialogComponent>;
    let service: EmploeeService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhapptestTestModule],
        declarations: [EmploeeDeleteDialogComponent]
      })
        .overrideTemplate(EmploeeDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(EmploeeDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(EmploeeService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
