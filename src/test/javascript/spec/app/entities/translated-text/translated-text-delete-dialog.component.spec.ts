import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { JhapptestTestModule } from '../../../test.module';
import { TranslatedTextDeleteDialogComponent } from 'app/entities/translated-text/translated-text-delete-dialog.component';
import { TranslatedTextService } from 'app/entities/translated-text/translated-text.service';

describe('Component Tests', () => {
  describe('TranslatedText Management Delete Component', () => {
    let comp: TranslatedTextDeleteDialogComponent;
    let fixture: ComponentFixture<TranslatedTextDeleteDialogComponent>;
    let service: TranslatedTextService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhapptestTestModule],
        declarations: [TranslatedTextDeleteDialogComponent]
      })
        .overrideTemplate(TranslatedTextDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TranslatedTextDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TranslatedTextService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
