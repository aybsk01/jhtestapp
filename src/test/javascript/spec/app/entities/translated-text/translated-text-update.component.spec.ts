import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { JhapptestTestModule } from '../../../test.module';
import { TranslatedTextUpdateComponent } from 'app/entities/translated-text/translated-text-update.component';
import { TranslatedTextService } from 'app/entities/translated-text/translated-text.service';
import { TranslatedText } from 'app/shared/model/translated-text.model';

describe('Component Tests', () => {
  describe('TranslatedText Management Update Component', () => {
    let comp: TranslatedTextUpdateComponent;
    let fixture: ComponentFixture<TranslatedTextUpdateComponent>;
    let service: TranslatedTextService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhapptestTestModule],
        declarations: [TranslatedTextUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(TranslatedTextUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TranslatedTextUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TranslatedTextService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new TranslatedText(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new TranslatedText();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
