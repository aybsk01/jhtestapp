import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { JhapptestTestModule } from '../../../test.module';
import { TranslatedTextComponent } from 'app/entities/translated-text/translated-text.component';
import { TranslatedTextService } from 'app/entities/translated-text/translated-text.service';
import { TranslatedText } from 'app/shared/model/translated-text.model';

describe('Component Tests', () => {
  describe('TranslatedText Management Component', () => {
    let comp: TranslatedTextComponent;
    let fixture: ComponentFixture<TranslatedTextComponent>;
    let service: TranslatedTextService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhapptestTestModule],
        declarations: [TranslatedTextComponent],
        providers: []
      })
        .overrideTemplate(TranslatedTextComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TranslatedTextComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TranslatedTextService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new TranslatedText(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.translatedTexts[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
