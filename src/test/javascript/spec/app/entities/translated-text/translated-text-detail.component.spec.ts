import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { JhapptestTestModule } from '../../../test.module';
import { TranslatedTextDetailComponent } from 'app/entities/translated-text/translated-text-detail.component';
import { TranslatedText } from 'app/shared/model/translated-text.model';

describe('Component Tests', () => {
  describe('TranslatedText Management Detail Component', () => {
    let comp: TranslatedTextDetailComponent;
    let fixture: ComponentFixture<TranslatedTextDetailComponent>;
    const route = ({ data: of({ translatedText: new TranslatedText(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhapptestTestModule],
        declarations: [TranslatedTextDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(TranslatedTextDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TranslatedTextDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.translatedText).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
