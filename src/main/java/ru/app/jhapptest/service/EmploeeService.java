package ru.app.jhapptest.service;

import ru.app.jhapptest.domain.Emploee;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Emploee}.
 */
public interface EmploeeService {

    /**
     * Save a emploee.
     *
     * @param emploee the entity to save.
     * @return the persisted entity.
     */
    Emploee save(Emploee emploee);

    /**
     * Get all the emploees.
     *
     * @return the list of entities.
     */
    List<Emploee> findAll();


    /**
     * Get the "id" emploee.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Emploee> findOne(Long id);

    /**
     * Delete the "id" emploee.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
