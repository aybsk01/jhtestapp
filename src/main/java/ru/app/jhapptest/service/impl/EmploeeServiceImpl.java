package ru.app.jhapptest.service.impl;

import ru.app.jhapptest.service.EmploeeService;
import ru.app.jhapptest.domain.Emploee;
import ru.app.jhapptest.repository.EmploeeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Emploee}.
 */
@Service
@Transactional
public class EmploeeServiceImpl implements EmploeeService {

    private final Logger log = LoggerFactory.getLogger(EmploeeServiceImpl.class);

    private final EmploeeRepository emploeeRepository;

    public EmploeeServiceImpl(EmploeeRepository emploeeRepository) {
        this.emploeeRepository = emploeeRepository;
    }

    /**
     * Save a emploee.
     *
     * @param emploee the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Emploee save(Emploee emploee) {
        log.debug("Request to save Emploee : {}", emploee);
        return emploeeRepository.save(emploee);
    }

    /**
     * Get all the emploees.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<Emploee> findAll() {
        log.debug("Request to get all Emploees");
        return emploeeRepository.findAll();
    }


    /**
     * Get one emploee by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Emploee> findOne(Long id) {
        log.debug("Request to get Emploee : {}", id);
        return emploeeRepository.findById(id);
    }

    /**
     * Delete the emploee by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Emploee : {}", id);
        emploeeRepository.deleteById(id);
    }
}
