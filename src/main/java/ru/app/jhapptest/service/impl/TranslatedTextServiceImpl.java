package ru.app.jhapptest.service.impl;

import ru.app.jhapptest.domain.enumeration.Language;
import ru.app.jhapptest.service.TranslatedTextService;
import ru.app.jhapptest.domain.TranslatedText;
import ru.app.jhapptest.repository.TranslatedTextRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import com.google.cloud.translate.Translate;
import com.google.cloud.translate.Translate.TranslateOption;
import com.google.cloud.translate.TranslateOptions;
import com.google.cloud.translate.Translation;

/**
 * Service Implementation for managing {@link TranslatedText}.
 */
@Service
@Transactional
public class TranslatedTextServiceImpl implements TranslatedTextService {

    private final Logger log = LoggerFactory.getLogger(TranslatedTextServiceImpl.class);

    private final TranslatedTextRepository translatedTextRepository;

    public TranslatedTextServiceImpl(TranslatedTextRepository translatedTextRepository) {
        this.translatedTextRepository = translatedTextRepository;
    }

    /**
     * Save a translatedText.
     *
     * @param translatedText the entity to save.
     * @return the persisted entity.
     */
    @Override
    public TranslatedText save(TranslatedText translatedText) {
        log.debug("Request to save TranslatedText : {}", translatedText);
        return translatedTextRepository.save(translatedText);
    }

    /**
     * Get all the translatedTexts.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<TranslatedText> findAll() {
        log.debug("Request to get all TranslatedTexts");
        return translatedTextRepository.findAll();
    }


    /**
     * Get one translatedText by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<TranslatedText> findOne(Long id) {
        log.debug("Request to get TranslatedText : {}", id);
        return translatedTextRepository.findById(id);
    }

    /**
     * Delete the translatedText by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TranslatedText : {}", id);
        translatedTextRepository.deleteById(id);
    }

    @Override
    public String getGoogleTranslation(TranslatedText textToTranslate){
        try {
            Translate translate = TranslateOptions.getDefaultInstance().getService();
            //translate.setKey("");
            Translation translation =
                translate.translate(
                    textToTranslate.getContent(),
                    TranslateOption.sourceLanguage(textToTranslate.getLanguage().equals(Language.RUSSIAN) ? "ru" : "en"),
                    TranslateOption.targetLanguage(textToTranslate.getLanguage().equals(Language.RUSSIAN) ? "ru" : "en"));
            return translation.getTranslatedText();
        }
        catch (Exception e){
            return e.toString();
        }
    }
}
