package ru.app.jhapptest.service.impl;

import ru.app.jhapptest.service.SourceTextService;
import ru.app.jhapptest.domain.SourceText;
import ru.app.jhapptest.repository.SourceTextRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link SourceText}.
 */
@Service
@Transactional
public class SourceTextServiceImpl implements SourceTextService {

    private final Logger log = LoggerFactory.getLogger(SourceTextServiceImpl.class);

    private final SourceTextRepository sourceTextRepository;

    public SourceTextServiceImpl(SourceTextRepository sourceTextRepository) {
        this.sourceTextRepository = sourceTextRepository;
    }

    /**
     * Save a sourceText.
     *
     * @param sourceText the entity to save.
     * @return the persisted entity.
     */
    @Override
    public SourceText save(SourceText sourceText) {
        log.debug("Request to save SourceText : {}", sourceText);
        return sourceTextRepository.save(sourceText);
    }

    /**
     * Get all the sourceTexts.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<SourceText> findAll() {
        log.debug("Request to get all SourceTexts");
        return sourceTextRepository.findAll();
    }


    /**
     * Get one sourceText by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SourceText> findOne(Long id) {
        log.debug("Request to get SourceText : {}", id);
        return sourceTextRepository.findById(id);
    }

    /**
     * Delete the sourceText by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SourceText : {}", id);
        sourceTextRepository.deleteById(id);
    }
}
