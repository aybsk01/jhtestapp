package ru.app.jhapptest.service;

import ru.app.jhapptest.domain.TranslatedText;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link TranslatedText}.
 */
public interface TranslatedTextService {

    /**
     * Save a translatedText.
     *
     * @param translatedText the entity to save.
     * @return the persisted entity.
     */
    TranslatedText save(TranslatedText translatedText);

    /**
     * Get all the translatedTexts.
     *
     * @return the list of entities.
     */
    List<TranslatedText> findAll();


    /**
     * Get the "id" translatedText.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TranslatedText> findOne(Long id);

    /**
     * Delete the "id" translatedText.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    String getGoogleTranslation(TranslatedText textToTranslate);
}
