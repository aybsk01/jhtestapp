package ru.app.jhapptest.service;

import ru.app.jhapptest.domain.SourceText;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link SourceText}.
 */
public interface SourceTextService {

    /**
     * Save a sourceText.
     *
     * @param sourceText the entity to save.
     * @return the persisted entity.
     */
    SourceText save(SourceText sourceText);

    /**
     * Get all the sourceTexts.
     *
     * @return the list of entities.
     */
    List<SourceText> findAll();


    /**
     * Get the "id" sourceText.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<SourceText> findOne(Long id);

    /**
     * Delete the "id" sourceText.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
