package ru.app.jhapptest.web.rest;

import ru.app.jhapptest.domain.Emploee;
import ru.app.jhapptest.service.EmploeeService;
import ru.app.jhapptest.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ru.app.jhapptest.domain.Emploee}.
 */
@RestController
@RequestMapping("/api")
public class EmploeeResource {

    private final Logger log = LoggerFactory.getLogger(EmploeeResource.class);

    private static final String ENTITY_NAME = "emploee";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EmploeeService emploeeService;

    public EmploeeResource(EmploeeService emploeeService) {
        this.emploeeService = emploeeService;
    }

    /**
     * {@code POST  /emploees} : Create a new emploee.
     *
     * @param emploee the emploee to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new emploee, or with status {@code 400 (Bad Request)} if the emploee has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/emploees")
    public ResponseEntity<Emploee> createEmploee(@RequestBody Emploee emploee) throws URISyntaxException {
        log.debug("REST request to save Emploee : {}", emploee);
        if (emploee.getId() != null) {
            throw new BadRequestAlertException("A new emploee cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Emploee result = emploeeService.save(emploee);
        return ResponseEntity.created(new URI("/api/emploees/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /emploees} : Updates an existing emploee.
     *
     * @param emploee the emploee to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated emploee,
     * or with status {@code 400 (Bad Request)} if the emploee is not valid,
     * or with status {@code 500 (Internal Server Error)} if the emploee couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/emploees")
    public ResponseEntity<Emploee> updateEmploee(@RequestBody Emploee emploee) throws URISyntaxException {
        log.debug("REST request to update Emploee : {}", emploee);
        if (emploee.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Emploee result = emploeeService.save(emploee);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, emploee.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /emploees} : get all the emploees.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of emploees in body.
     */
    @GetMapping("/emploees")
    public List<Emploee> getAllEmploees() {
        log.debug("REST request to get all Emploees");
        return emploeeService.findAll();
    }

    /**
     * {@code GET  /emploees/:id} : get the "id" emploee.
     *
     * @param id the id of the emploee to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the emploee, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/emploees/{id}")
    public ResponseEntity<Emploee> getEmploee(@PathVariable Long id) {
        log.debug("REST request to get Emploee : {}", id);
        Optional<Emploee> emploee = emploeeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(emploee);
    }

    /**
     * {@code DELETE  /emploees/:id} : delete the "id" emploee.
     *
     * @param id the id of the emploee to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/emploees/{id}")
    public ResponseEntity<Void> deleteEmploee(@PathVariable Long id) {
        log.debug("REST request to delete Emploee : {}", id);
        emploeeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
