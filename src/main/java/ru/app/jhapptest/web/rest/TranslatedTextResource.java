package ru.app.jhapptest.web.rest;

import ru.app.jhapptest.domain.TranslatedText;
import ru.app.jhapptest.service.TranslatedTextService;
import ru.app.jhapptest.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ru.app.jhapptest.domain.TranslatedText}.
 */
@RestController
@RequestMapping("/api")
public class TranslatedTextResource {

    private final Logger log = LoggerFactory.getLogger(TranslatedTextResource.class);

    private static final String ENTITY_NAME = "translatedText";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TranslatedTextService translatedTextService;

    public TranslatedTextResource(TranslatedTextService translatedTextService) {
        this.translatedTextService = translatedTextService;
    }

    /**
     * {@code POST  /translated-texts} : Create a new translatedText.
     *
     * @param translatedText the translatedText to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new translatedText, or with status {@code 400 (Bad Request)} if the translatedText has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/translated-texts")
    public ResponseEntity<TranslatedText> createTranslatedText(@RequestBody TranslatedText translatedText) throws URISyntaxException {
        log.debug("REST request to save TranslatedText : {}", translatedText);
        if (translatedText.getId() != null) {
            throw new BadRequestAlertException("A new translatedText cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TranslatedText result = translatedTextService.save(translatedText);
        return ResponseEntity.created(new URI("/api/translated-texts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /translated-texts} : Updates an existing translatedText.
     *
     * @param translatedText the translatedText to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated translatedText,
     * or with status {@code 400 (Bad Request)} if the translatedText is not valid,
     * or with status {@code 500 (Internal Server Error)} if the translatedText couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/translated-texts")
    public ResponseEntity<TranslatedText> updateTranslatedText(@RequestBody TranslatedText translatedText) throws URISyntaxException {
        log.debug("REST request to update TranslatedText : {}", translatedText);
        if (translatedText.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TranslatedText result = translatedTextService.save(translatedText);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, translatedText.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /translated-texts} : get all the translatedTexts.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of translatedTexts in body.
     */
    @GetMapping("/translated-texts")
    public List<TranslatedText> getAllTranslatedTexts() {
        log.debug("REST request to get all TranslatedTexts");
        return translatedTextService.findAll();
    }

    /**
     * {@code GET  /translated-texts/:id} : get the "id" translatedText.
     *
     * @param id the id of the translatedText to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the translatedText, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/translated-texts/{id}")
    public ResponseEntity<TranslatedText> getTranslatedText(@PathVariable Long id) {
        log.debug("REST request to get TranslatedText : {}", id);
        Optional<TranslatedText> translatedText = translatedTextService.findOne(id);
        return ResponseUtil.wrapOrNotFound(translatedText);
    }

    /**
     * {@code DELETE  /translated-texts/:id} : delete the "id" translatedText.
     *
     * @param id the id of the translatedText to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/translated-texts/{id}")
    public ResponseEntity<Void> deleteTranslatedText(@PathVariable Long id) {
        log.debug("REST request to delete TranslatedText : {}", id);
        translatedTextService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    @PostMapping("/googletranslation")
    public ResponseEntity<TranslatedText> getGoogleTranslation(@RequestBody TranslatedText target){
        //Потестировать, какие ошибки могут быть
        String translatedText = translatedTextService.getGoogleTranslation(target);
        TranslatedText tt = new TranslatedText();
        tt.setContent(translatedText);
        return ResponseEntity.ok(tt);
    }
}
