package ru.app.jhapptest.web.rest;

import ru.app.jhapptest.domain.SourceText;
import ru.app.jhapptest.service.SourceTextService;
import ru.app.jhapptest.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ru.app.jhapptest.domain.SourceText}.
 */
@RestController
@RequestMapping("/api")
public class SourceTextResource {

    private final Logger log = LoggerFactory.getLogger(SourceTextResource.class);

    private static final String ENTITY_NAME = "sourceText";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SourceTextService sourceTextService;

    public SourceTextResource(SourceTextService sourceTextService) {
        this.sourceTextService = sourceTextService;
    }

    /**
     * {@code POST  /source-texts} : Create a new sourceText.
     *
     * @param sourceText the sourceText to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new sourceText, or with status {@code 400 (Bad Request)} if the sourceText has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/source-texts")
    public ResponseEntity<SourceText> createSourceText(@RequestBody SourceText sourceText) throws URISyntaxException {
        log.debug("REST request to save SourceText : {}", sourceText);
        if (sourceText.getId() != null) {
            throw new BadRequestAlertException("A new sourceText cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SourceText result = sourceTextService.save(sourceText);
        return ResponseEntity.created(new URI("/api/source-texts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /source-texts} : Updates an existing sourceText.
     *
     * @param sourceText the sourceText to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated sourceText,
     * or with status {@code 400 (Bad Request)} if the sourceText is not valid,
     * or with status {@code 500 (Internal Server Error)} if the sourceText couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/source-texts")
    public ResponseEntity<SourceText> updateSourceText(@RequestBody SourceText sourceText) throws URISyntaxException {
        log.debug("REST request to update SourceText : {}", sourceText);
        if (sourceText.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SourceText result = sourceTextService.save(sourceText);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, sourceText.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /source-texts} : get all the sourceTexts.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of sourceTexts in body.
     */
    @GetMapping("/source-texts")
    public List<SourceText> getAllSourceTexts() {
        log.debug("REST request to get all SourceTexts");
        return sourceTextService.findAll();
    }

    /**
     * {@code GET  /source-texts/:id} : get the "id" sourceText.
     *
     * @param id the id of the sourceText to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the sourceText, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/source-texts/{id}")
    public ResponseEntity<SourceText> getSourceText(@PathVariable Long id) {
        log.debug("REST request to get SourceText : {}", id);
        Optional<SourceText> sourceText = sourceTextService.findOne(id);
        return ResponseUtil.wrapOrNotFound(sourceText);
    }

    /**
     * {@code DELETE  /source-texts/:id} : delete the "id" sourceText.
     *
     * @param id the id of the sourceText to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/source-texts/{id}")
    public ResponseEntity<Void> deleteSourceText(@PathVariable Long id) {
        log.debug("REST request to delete SourceText : {}", id);
        sourceTextService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
