package ru.app.jhapptest.repository;
import ru.app.jhapptest.domain.Emploee;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Emploee entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EmploeeRepository extends JpaRepository<Emploee, Long> {

}
