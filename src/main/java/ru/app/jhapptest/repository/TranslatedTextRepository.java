package ru.app.jhapptest.repository;
import ru.app.jhapptest.domain.TranslatedText;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TranslatedText entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TranslatedTextRepository extends JpaRepository<TranslatedText, Long> {

}
