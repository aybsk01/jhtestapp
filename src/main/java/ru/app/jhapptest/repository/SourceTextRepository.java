package ru.app.jhapptest.repository;
import ru.app.jhapptest.domain.SourceText;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SourceText entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SourceTextRepository extends JpaRepository<SourceText, Long> {

}
