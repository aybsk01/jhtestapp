package ru.app.jhapptest.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Client.
 */
@Entity
@Table(name = "client")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "fio")
    private String fio;

    @OneToMany(mappedBy = "client")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<SourceText> sourceTexts = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFio() {
        return fio;
    }

    public Client fio(String fio) {
        this.fio = fio;
        return this;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public Set<SourceText> getSourceTexts() {
        return sourceTexts;
    }

    public Client sourceTexts(Set<SourceText> sourceTexts) {
        this.sourceTexts = sourceTexts;
        return this;
    }

    public Client addSourceText(SourceText sourceText) {
        this.sourceTexts.add(sourceText);
        sourceText.setClient(this);
        return this;
    }

    public Client removeSourceText(SourceText sourceText) {
        this.sourceTexts.remove(sourceText);
        sourceText.setClient(null);
        return this;
    }

    public void setSourceTexts(Set<SourceText> sourceTexts) {
        this.sourceTexts = sourceTexts;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Client)) {
            return false;
        }
        return id != null && id.equals(((Client) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Client{" +
            "id=" + getId() +
            ", fio='" + getFio() + "'" +
            "}";
    }
}
