package ru.app.jhapptest.domain.enumeration;

/**
 * The Language enumeration.
 */
public enum Language {
    ENGLISH, RUSSIAN
}
