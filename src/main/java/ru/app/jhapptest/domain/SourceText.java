package ru.app.jhapptest.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A SourceText.
 */
@Entity
@Table(name = "source_text")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SourceText implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "content")
    private String content;

    @ManyToOne
    @JsonIgnoreProperties("sourceTexts")
    private Client client;

    @ManyToOne
    @JsonIgnoreProperties("sourceTexts")
    private Emploee emploee;

    @OneToMany(mappedBy = "sourceText")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<TranslatedText> translatedTexts = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public SourceText title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public SourceText content(String content) {
        this.content = content;
        return this;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Client getClient() {
        return client;
    }

    public SourceText client(Client client) {
        this.client = client;
        return this;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Emploee getEmploee() {
        return emploee;
    }

    public SourceText emploee(Emploee emploee) {
        this.emploee = emploee;
        return this;
    }

    public void setEmploee(Emploee emploee) {
        this.emploee = emploee;
    }

    public Set<TranslatedText> getTranslatedTexts() {
        return translatedTexts;
    }

    public SourceText translatedTexts(Set<TranslatedText> translatedTexts) {
        this.translatedTexts = translatedTexts;
        return this;
    }

    public SourceText addTranslatedText(TranslatedText translatedText) {
        this.translatedTexts.add(translatedText);
        translatedText.setSourceText(this);
        return this;
    }

    public SourceText removeTranslatedText(TranslatedText translatedText) {
        this.translatedTexts.remove(translatedText);
        translatedText.setSourceText(null);
        return this;
    }

    public void setTranslatedTexts(Set<TranslatedText> translatedTexts) {
        this.translatedTexts = translatedTexts;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SourceText)) {
            return false;
        }
        return id != null && id.equals(((SourceText) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "SourceText{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", content='" + getContent() + "'" +
            "}";
    }
}
