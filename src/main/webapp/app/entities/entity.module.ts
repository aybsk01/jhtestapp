import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'client',
        loadChildren: () => import('./client/client.module').then(m => m.JhapptestClientModule)
      },
      {
        path: 'emploee',
        loadChildren: () => import('./emploee/emploee.module').then(m => m.JhapptestEmploeeModule)
      },
      {
        path: 'source-text',
        loadChildren: () => import('./source-text/source-text.module').then(m => m.JhapptestSourceTextModule)
      },
      {
        path: 'translated-text',
        loadChildren: () => import('./translated-text/translated-text.module').then(m => m.JhapptestTranslatedTextModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class JhapptestEntityModule {}
