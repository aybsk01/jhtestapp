import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ISourceText, SourceText } from 'app/shared/model/source-text.model';
import { SourceTextService } from './source-text.service';
import { IClient } from 'app/shared/model/client.model';
import { ClientService } from 'app/entities/client/client.service';
import { IEmploee } from 'app/shared/model/emploee.model';
import { EmploeeService } from 'app/entities/emploee/emploee.service';

@Component({
  selector: 'jhi-source-text-update',
  templateUrl: './source-text-update.component.html'
})
export class SourceTextUpdateComponent implements OnInit {
  isSaving: boolean;

  clients: IClient[];

  emploees: IEmploee[];

  editForm = this.fb.group({
    id: [],
    title: [],
    content: [],
    client: [],
    emploee: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected sourceTextService: SourceTextService,
    protected clientService: ClientService,
    protected emploeeService: EmploeeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ sourceText }) => {
      this.updateForm(sourceText);
    });
    this.clientService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IClient[]>) => mayBeOk.ok),
        map((response: HttpResponse<IClient[]>) => response.body)
      )
      .subscribe((res: IClient[]) => (this.clients = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.emploeeService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IEmploee[]>) => mayBeOk.ok),
        map((response: HttpResponse<IEmploee[]>) => response.body)
      )
      .subscribe((res: IEmploee[]) => (this.emploees = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(sourceText: ISourceText) {
    this.editForm.patchValue({
      id: sourceText.id,
      title: sourceText.title,
      content: sourceText.content,
      client: sourceText.client,
      emploee: sourceText.emploee
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const sourceText = this.createFromForm();
    if (sourceText.id !== undefined) {
      this.subscribeToSaveResponse(this.sourceTextService.update(sourceText));
    } else {
      this.subscribeToSaveResponse(this.sourceTextService.create(sourceText));
    }
  }

  private createFromForm(): ISourceText {
    return {
      ...new SourceText(),
      id: this.editForm.get(['id']).value,
      title: this.editForm.get(['title']).value,
      content: this.editForm.get(['content']).value,
      client: this.editForm.get(['client']).value,
      emploee: this.editForm.get(['emploee']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISourceText>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackClientById(index: number, item: IClient) {
    return item.id;
  }

  trackEmploeeById(index: number, item: IEmploee) {
    return item.id;
  }
}
