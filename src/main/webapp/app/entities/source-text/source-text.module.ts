import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JhapptestSharedModule } from 'app/shared/shared.module';
import { SourceTextComponent } from './source-text.component';
import { SourceTextDetailComponent } from './source-text-detail.component';
import { SourceTextUpdateComponent } from './source-text-update.component';
import { SourceTextDeletePopupComponent, SourceTextDeleteDialogComponent } from './source-text-delete-dialog.component';
import { sourceTextRoute, sourceTextPopupRoute } from './source-text.route';

const ENTITY_STATES = [...sourceTextRoute, ...sourceTextPopupRoute];

@NgModule({
  imports: [JhapptestSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    SourceTextComponent,
    SourceTextDetailComponent,
    SourceTextUpdateComponent,
    SourceTextDeleteDialogComponent,
    SourceTextDeletePopupComponent
  ],
  entryComponents: [SourceTextDeleteDialogComponent]
})
export class JhapptestSourceTextModule {}
