import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISourceText } from 'app/shared/model/source-text.model';

@Component({
  selector: 'jhi-source-text-detail',
  templateUrl: './source-text-detail.component.html'
})
export class SourceTextDetailComponent implements OnInit {
  sourceText: ISourceText;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ sourceText }) => {
      this.sourceText = sourceText;
    });
  }

  previousState() {
    window.history.back();
  }
}
