import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { SourceText } from 'app/shared/model/source-text.model';
import { SourceTextService } from './source-text.service';
import { SourceTextComponent } from './source-text.component';
import { SourceTextDetailComponent } from './source-text-detail.component';
import { SourceTextUpdateComponent } from './source-text-update.component';
import { SourceTextDeletePopupComponent } from './source-text-delete-dialog.component';
import { ISourceText } from 'app/shared/model/source-text.model';

@Injectable({ providedIn: 'root' })
export class SourceTextResolve implements Resolve<ISourceText> {
  constructor(private service: SourceTextService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ISourceText> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<SourceText>) => response.ok),
        map((sourceText: HttpResponse<SourceText>) => sourceText.body)
      );
    }
    return of(new SourceText());
  }
}

export const sourceTextRoute: Routes = [
  {
    path: '',
    component: SourceTextComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'SourceTexts'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: SourceTextDetailComponent,
    resolve: {
      sourceText: SourceTextResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'SourceTexts'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: SourceTextUpdateComponent,
    resolve: {
      sourceText: SourceTextResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'SourceTexts'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: SourceTextUpdateComponent,
    resolve: {
      sourceText: SourceTextResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'SourceTexts'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const sourceTextPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: SourceTextDeletePopupComponent,
    resolve: {
      sourceText: SourceTextResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'SourceTexts'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
