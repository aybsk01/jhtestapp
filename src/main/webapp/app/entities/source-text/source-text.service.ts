import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ISourceText } from 'app/shared/model/source-text.model';

type EntityResponseType = HttpResponse<ISourceText>;
type EntityArrayResponseType = HttpResponse<ISourceText[]>;

@Injectable({ providedIn: 'root' })
export class SourceTextService {
  public resourceUrl = SERVER_API_URL + 'api/source-texts';

  constructor(protected http: HttpClient) {}

  create(sourceText: ISourceText): Observable<EntityResponseType> {
    return this.http.post<ISourceText>(this.resourceUrl, sourceText, { observe: 'response' });
  }

  update(sourceText: ISourceText): Observable<EntityResponseType> {
    return this.http.put<ISourceText>(this.resourceUrl, sourceText, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ISourceText>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISourceText[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
