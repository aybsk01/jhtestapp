import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISourceText } from 'app/shared/model/source-text.model';
import { SourceTextService } from './source-text.service';

@Component({
  selector: 'jhi-source-text-delete-dialog',
  templateUrl: './source-text-delete-dialog.component.html'
})
export class SourceTextDeleteDialogComponent {
  sourceText: ISourceText;

  constructor(
    protected sourceTextService: SourceTextService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.sourceTextService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'sourceTextListModification',
        content: 'Deleted an sourceText'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-source-text-delete-popup',
  template: ''
})
export class SourceTextDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ sourceText }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(SourceTextDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.sourceText = sourceText;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/source-text', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/source-text', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
