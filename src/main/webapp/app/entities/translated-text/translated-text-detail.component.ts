import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITranslatedText } from 'app/shared/model/translated-text.model';

@Component({
  selector: 'jhi-translated-text-detail',
  templateUrl: './translated-text-detail.component.html'
})
export class TranslatedTextDetailComponent implements OnInit {
  translatedText: ITranslatedText;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ translatedText }) => {
      this.translatedText = translatedText;
    });
  }

  previousState() {
    window.history.back();
  }
}
