import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { TranslatedText } from 'app/shared/model/translated-text.model';
import { TranslatedTextService } from './translated-text.service';
import { TranslatedTextComponent } from './translated-text.component';
import { TranslatedTextDetailComponent } from './translated-text-detail.component';
import { TranslatedTextUpdateComponent } from './translated-text-update.component';
import { TranslatedTextDeletePopupComponent } from './translated-text-delete-dialog.component';
import { ITranslatedText } from 'app/shared/model/translated-text.model';

@Injectable({ providedIn: 'root' })
export class TranslatedTextResolve implements Resolve<ITranslatedText> {
  constructor(private service: TranslatedTextService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ITranslatedText> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<TranslatedText>) => response.ok),
        map((translatedText: HttpResponse<TranslatedText>) => translatedText.body)
      );
    }
    return of(new TranslatedText());
  }
}

export const translatedTextRoute: Routes = [
  {
    path: '',
    component: TranslatedTextComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TranslatedTexts'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: TranslatedTextDetailComponent,
    resolve: {
      translatedText: TranslatedTextResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TranslatedTexts'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: TranslatedTextUpdateComponent,
    resolve: {
      translatedText: TranslatedTextResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TranslatedTexts'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: TranslatedTextUpdateComponent,
    resolve: {
      translatedText: TranslatedTextResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TranslatedTexts'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const translatedTextPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: TranslatedTextDeletePopupComponent,
    resolve: {
      translatedText: TranslatedTextResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TranslatedTexts'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
