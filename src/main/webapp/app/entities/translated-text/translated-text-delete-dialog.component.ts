import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITranslatedText } from 'app/shared/model/translated-text.model';
import { TranslatedTextService } from './translated-text.service';

@Component({
  selector: 'jhi-translated-text-delete-dialog',
  templateUrl: './translated-text-delete-dialog.component.html'
})
export class TranslatedTextDeleteDialogComponent {
  translatedText: ITranslatedText;

  constructor(
    protected translatedTextService: TranslatedTextService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.translatedTextService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'translatedTextListModification',
        content: 'Deleted an translatedText'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-translated-text-delete-popup',
  template: ''
})
export class TranslatedTextDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ translatedText }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(TranslatedTextDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.translatedText = translatedText;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/translated-text', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/translated-text', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
