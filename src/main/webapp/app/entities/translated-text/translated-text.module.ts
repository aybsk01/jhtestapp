import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JhapptestSharedModule } from 'app/shared/shared.module';
import { TranslatedTextComponent } from './translated-text.component';
import { TranslatedTextDetailComponent } from './translated-text-detail.component';
import { TranslatedTextUpdateComponent } from './translated-text-update.component';
import { TranslatedTextDeletePopupComponent, TranslatedTextDeleteDialogComponent } from './translated-text-delete-dialog.component';
import { translatedTextRoute, translatedTextPopupRoute } from './translated-text.route';

const ENTITY_STATES = [...translatedTextRoute, ...translatedTextPopupRoute];

@NgModule({
  imports: [JhapptestSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    TranslatedTextComponent,
    TranslatedTextDetailComponent,
    TranslatedTextUpdateComponent,
    TranslatedTextDeleteDialogComponent,
    TranslatedTextDeletePopupComponent
  ],
  entryComponents: [TranslatedTextDeleteDialogComponent]
})
export class JhapptestTranslatedTextModule {}
