import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ITranslatedText } from 'app/shared/model/translated-text.model';

type EntityResponseType = HttpResponse<ITranslatedText>;
type EntityArrayResponseType = HttpResponse<ITranslatedText[]>;

@Injectable({ providedIn: 'root' })
export class TranslatedTextService {
  public resourceUrl = SERVER_API_URL + 'api/translated-texts';
  public googleTranslationUrl = SERVER_API_URL + 'api/googletranslation';

  constructor(protected http: HttpClient) {}

  create(translatedText: ITranslatedText): Observable<EntityResponseType> {
    return this.http.post<ITranslatedText>(this.resourceUrl, translatedText, { observe: 'response' });
  }

  update(translatedText: ITranslatedText): Observable<EntityResponseType> {
    return this.http.put<ITranslatedText>(this.resourceUrl, translatedText, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ITranslatedText>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITranslatedText[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  googleTranslation(translations: ITranslatedText): Observable<EntityResponseType> {
    return this.http.post<ITranslatedText>(this.googleTranslationUrl, translations, { observe: 'response' });
  }
}
