import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ITranslatedText } from 'app/shared/model/translated-text.model';
import { AccountService } from 'app/core/auth/account.service';
import { TranslatedTextService } from './translated-text.service';

@Component({
  selector: 'jhi-translated-text',
  templateUrl: './translated-text.component.html'
})
export class TranslatedTextComponent implements OnInit, OnDestroy {
  translatedTexts: ITranslatedText[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected translatedTextService: TranslatedTextService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.translatedTextService
      .query()
      .pipe(
        filter((res: HttpResponse<ITranslatedText[]>) => res.ok),
        map((res: HttpResponse<ITranslatedText[]>) => res.body)
      )
      .subscribe(
        (res: ITranslatedText[]) => {
          this.translatedTexts = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInTranslatedTexts();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ITranslatedText) {
    return item.id;
  }

  registerChangeInTranslatedTexts() {
    this.eventSubscriber = this.eventManager.subscribe('translatedTextListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
