import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ITranslatedText, TranslatedText } from 'app/shared/model/translated-text.model';
import { TranslatedTextService } from './translated-text.service';
import { ISourceText } from 'app/shared/model/source-text.model';
import { SourceTextService } from 'app/entities/source-text/source-text.service';

@Component({
  selector: 'jhi-translated-text-update',
  templateUrl: './translated-text-update.component.html'
})
export class TranslatedTextUpdateComponent implements OnInit {
  isSaving: boolean;
  isTranslating: boolean;

  sourcetexts: ISourceText[];
  selectedText : ISourceText;

  editForm = this.fb.group({
    id: [],
    content: [],
    sourcetextcontent : [],
    language: [],
    sourceText: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected translatedTextService: TranslatedTextService,
    protected sourceTextService: SourceTextService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.isTranslating = false;
    this.activatedRoute.data.subscribe(({ translatedText }) => {
      this.updateForm(translatedText);
    });
    this.sourceTextService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ISourceText[]>) => mayBeOk.ok),
        map((response: HttpResponse<ISourceText[]>) => response.body)
      )
      .subscribe((res: ISourceText[]) => (this.sourcetexts = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  get selectedTextMod() {
    return this.selectedText;
  }

  set selectedTextMod(value) {
    this.selectedText = value;
    this.editForm.patchValue({sourcetextcontent : this.selectedText.content})
  }

  getTextTranslation(){
    this.isTranslating = true;
    this.subscribeToGoogleTranslation(this.translatedTextService.googleTranslation(
      {
        ...new TranslatedText(),
        content: this.selectedText.content,
        language: this.editForm.get(['language']).value
      }
    ));
  }

  updateForm(translatedText: ITranslatedText) {
    this.selectedText = translatedText.sourceText;
    this.editForm.patchValue({
      id: translatedText.id,
      content: translatedText.content,
      language: translatedText.language,
      sourceText: translatedText.sourceText
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const translatedText = this.createFromForm();
    if (translatedText.id !== undefined) {
      this.subscribeToSaveResponse(this.translatedTextService.update(translatedText));
    } else {
      this.subscribeToSaveResponse(this.translatedTextService.create(translatedText));
    }
  }

  private createFromForm(): ITranslatedText {
    return {
      ...new TranslatedText(),
      id: this.editForm.get(['id']).value,
      content: this.editForm.get(['content']).value,
      language: this.editForm.get(['language']).value,
      sourceText: this.editForm.get(['sourceText']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITranslatedText>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  protected subscribeToGoogleTranslation(result: Observable<HttpResponse<ITranslatedText>>) {
    result.subscribe((resp : HttpResponse<ITranslatedText>) => this.onTranslateSuccess(resp), () => this.onTranslateError());
  }

  protected onTranslateSuccess(resp : HttpResponse<ITranslatedText>) {
    this.isTranslating = false;
    this.editForm.patchValue({content : resp.body.content})
  }

  protected onTranslateError() {
    this.isTranslating = false;
    this.isSaving = false;
  }


  trackSourceTextById(index: number, item: ISourceText) {
    return item.id;
  }
}
