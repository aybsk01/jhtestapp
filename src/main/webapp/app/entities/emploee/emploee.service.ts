import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IEmploee } from 'app/shared/model/emploee.model';

type EntityResponseType = HttpResponse<IEmploee>;
type EntityArrayResponseType = HttpResponse<IEmploee[]>;

@Injectable({ providedIn: 'root' })
export class EmploeeService {
  public resourceUrl = SERVER_API_URL + 'api/emploees';

  constructor(protected http: HttpClient) {}

  create(emploee: IEmploee): Observable<EntityResponseType> {
    return this.http.post<IEmploee>(this.resourceUrl, emploee, { observe: 'response' });
  }

  update(emploee: IEmploee): Observable<EntityResponseType> {
    return this.http.put<IEmploee>(this.resourceUrl, emploee, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IEmploee>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IEmploee[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
