import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IEmploee, Emploee } from 'app/shared/model/emploee.model';
import { EmploeeService } from './emploee.service';

@Component({
  selector: 'jhi-emploee-update',
  templateUrl: './emploee-update.component.html'
})
export class EmploeeUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    fio: []
  });

  constructor(protected emploeeService: EmploeeService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ emploee }) => {
      this.updateForm(emploee);
    });
  }

  updateForm(emploee: IEmploee) {
    this.editForm.patchValue({
      id: emploee.id,
      fio: emploee.fio
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const emploee = this.createFromForm();
    if (emploee.id !== undefined) {
      this.subscribeToSaveResponse(this.emploeeService.update(emploee));
    } else {
      this.subscribeToSaveResponse(this.emploeeService.create(emploee));
    }
  }

  private createFromForm(): IEmploee {
    return {
      ...new Emploee(),
      id: this.editForm.get(['id']).value,
      fio: this.editForm.get(['fio']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEmploee>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
