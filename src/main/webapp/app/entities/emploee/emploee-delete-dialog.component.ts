import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IEmploee } from 'app/shared/model/emploee.model';
import { EmploeeService } from './emploee.service';

@Component({
  selector: 'jhi-emploee-delete-dialog',
  templateUrl: './emploee-delete-dialog.component.html'
})
export class EmploeeDeleteDialogComponent {
  emploee: IEmploee;

  constructor(protected emploeeService: EmploeeService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.emploeeService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'emploeeListModification',
        content: 'Deleted an emploee'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-emploee-delete-popup',
  template: ''
})
export class EmploeeDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ emploee }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(EmploeeDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.emploee = emploee;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/emploee', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/emploee', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
