import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IEmploee } from 'app/shared/model/emploee.model';

@Component({
  selector: 'jhi-emploee-detail',
  templateUrl: './emploee-detail.component.html'
})
export class EmploeeDetailComponent implements OnInit {
  emploee: IEmploee;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ emploee }) => {
      this.emploee = emploee;
    });
  }

  previousState() {
    window.history.back();
  }
}
