import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Emploee } from 'app/shared/model/emploee.model';
import { EmploeeService } from './emploee.service';
import { EmploeeComponent } from './emploee.component';
import { EmploeeDetailComponent } from './emploee-detail.component';
import { EmploeeUpdateComponent } from './emploee-update.component';
import { EmploeeDeletePopupComponent } from './emploee-delete-dialog.component';
import { IEmploee } from 'app/shared/model/emploee.model';

@Injectable({ providedIn: 'root' })
export class EmploeeResolve implements Resolve<IEmploee> {
  constructor(private service: EmploeeService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IEmploee> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Emploee>) => response.ok),
        map((emploee: HttpResponse<Emploee>) => emploee.body)
      );
    }
    return of(new Emploee());
  }
}

export const emploeeRoute: Routes = [
  {
    path: '',
    component: EmploeeComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Emploees'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: EmploeeDetailComponent,
    resolve: {
      emploee: EmploeeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Emploees'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: EmploeeUpdateComponent,
    resolve: {
      emploee: EmploeeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Emploees'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: EmploeeUpdateComponent,
    resolve: {
      emploee: EmploeeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Emploees'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const emploeePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: EmploeeDeletePopupComponent,
    resolve: {
      emploee: EmploeeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Emploees'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
