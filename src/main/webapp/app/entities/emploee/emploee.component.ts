import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IEmploee } from 'app/shared/model/emploee.model';
import { AccountService } from 'app/core/auth/account.service';
import { EmploeeService } from './emploee.service';

@Component({
  selector: 'jhi-emploee',
  templateUrl: './emploee.component.html'
})
export class EmploeeComponent implements OnInit, OnDestroy {
  emploees: IEmploee[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected emploeeService: EmploeeService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.emploeeService
      .query()
      .pipe(
        filter((res: HttpResponse<IEmploee[]>) => res.ok),
        map((res: HttpResponse<IEmploee[]>) => res.body)
      )
      .subscribe(
        (res: IEmploee[]) => {
          this.emploees = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInEmploees();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IEmploee) {
    return item.id;
  }

  registerChangeInEmploees() {
    this.eventSubscriber = this.eventManager.subscribe('emploeeListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
