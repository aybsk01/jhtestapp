import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JhapptestSharedModule } from 'app/shared/shared.module';
import { EmploeeComponent } from './emploee.component';
import { EmploeeDetailComponent } from './emploee-detail.component';
import { EmploeeUpdateComponent } from './emploee-update.component';
import { EmploeeDeletePopupComponent, EmploeeDeleteDialogComponent } from './emploee-delete-dialog.component';
import { emploeeRoute, emploeePopupRoute } from './emploee.route';

const ENTITY_STATES = [...emploeeRoute, ...emploeePopupRoute];

@NgModule({
  imports: [JhapptestSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    EmploeeComponent,
    EmploeeDetailComponent,
    EmploeeUpdateComponent,
    EmploeeDeleteDialogComponent,
    EmploeeDeletePopupComponent
  ],
  entryComponents: [EmploeeDeleteDialogComponent]
})
export class JhapptestEmploeeModule {}
