import { ISourceText } from 'app/shared/model/source-text.model';
import { Language } from 'app/shared/model/enumerations/language.model';

export interface ITranslatedText {
  id?: number;
  content?: string;
  language?: Language;
  sourceText?: ISourceText;
}

export class TranslatedText implements ITranslatedText {
  constructor(public id?: number, public content?: string, public language?: Language, public sourceText?: ISourceText) {}
}
