import { IClient } from 'app/shared/model/client.model';
import { IEmploee } from 'app/shared/model/emploee.model';
import { ITranslatedText } from 'app/shared/model/translated-text.model';

export interface ISourceText {
  id?: number;
  title?: string;
  content?: string;
  client?: IClient;
  emploee?: IEmploee;
  translatedTexts?: ITranslatedText[];
}

export class SourceText implements ISourceText {
  constructor(
    public id?: number,
    public title?: string,
    public content?: string,
    public client?: IClient,
    public emploee?: IEmploee,
    public translatedTexts?: ITranslatedText[]
  ) {}
}
