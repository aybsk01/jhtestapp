import { ISourceText } from 'app/shared/model/source-text.model';

export interface IClient {
  id?: number;
  fio?: string;
  sourceTexts?: ISourceText[];
}

export class Client implements IClient {
  constructor(public id?: number, public fio?: string, public sourceTexts?: ISourceText[]) {}
}
