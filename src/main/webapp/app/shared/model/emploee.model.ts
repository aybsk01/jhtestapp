import { ISourceText } from 'app/shared/model/source-text.model';

export interface IEmploee {
  id?: number;
  fio?: string;
  sourceTexts?: ISourceText[];
}

export class Emploee implements IEmploee {
  constructor(public id?: number, public fio?: string, public sourceTexts?: ISourceText[]) {}
}
